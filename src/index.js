import './app.scss';

import {module} from 'angular';
import './home';

export default module('myApp', ['myApp.home']);
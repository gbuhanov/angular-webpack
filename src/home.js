import angular from 'angular';

angular
  .module('myApp.home', [])
  .controller('myCtrl', function($scope) {
    $scope.firstName = "John";
    $scope.lastName = "Doe 2";
  });